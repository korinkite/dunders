#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Rebuild strings into UltSnips-compatible snippets.'''

# IMPORT STANDARD LIBRARIES
import string
import re


class UltiSnipsTabstopFormatter(string.Formatter):

    '''This class converts formated text into UltiSnips-compatible text.'''


    def __init__(self, *args, **kwargs):
        '''Initialize the object instance.

        Args:
            *args: The objects needed by this class's base implementation
            *kwargs: The objects needed by this class's base implementation

        '''
        super(UltiSnipsTabstopFormatter, self).__init__(*args, **kwargs)
        self.digit_comp = re.compile('{(?P<digit>\d+)}')
        self.key_info = \
            {
                'keyValues': dict(),
                'currentMax': 1,
                'numbersUsed': set(),
            }

    def vformat(self, format_string, args, kwargs):
        '''Main convert function which converts from Python to UltiSnips.

        Args:
            format_string (str): The full string pattern to-be formatted
            *args: The objects needed by this class's base implementation
            *kwargs: The objects needed by this class's base implementation

        Returns:
            str: The formatted string

        '''
        digits = self.digit_comp.findall(format_string)
        for digit in digits:
            self.key_info['numbersUsed'].add(int(digit))

        return super(UltiSnipsTabstopFormatter, self).vformat(
            format_string, args, kwargs)

    def get_unused_number(self, number):
        '''Find the next number that hasn't been used yet.

        Args:
            number (int): The number to start counting up from

        Retruns:
            int: The next-available number

        '''
        number = int(number)
        while number in self.key_info['numbersUsed']:
            number += 1

        return number

    def get_keyword_value(self, key):
        '''Convert the given string to its UltiSnips representation.

        Args:
            key (str): The format string to get the UltiSnips keyword of

        Returns:
            str: A string in the form of '{#:some_string}'

        '''
        cur_max = self.key_info['currentMax']
        cur_max = self.get_unused_number(number=cur_max)

        self.key_info['keyValues'][key] = cur_max
        self.key_info['numbersUsed'].add(cur_max)

        return '${%d:%s}' % (self.key_info['keyValues'][key], key)

    def get_value(self, key, args, kwargs):
        '''Convert any given key to its UltiSnip-equivalent string.

        Args:
            key (str or int): If an int is given, the number is assumed to
                be valid and passed directly to the translator. If the key
                is a str a few things happen. 1. If the str has been used
                at some point before, its value is retrieved. 2. If this is the
                first time the keyword has been used, the next-available number
                is found and assigned to it.
            args: The args that get passed to the base implementation
            kwargs: The args that get passed to the base implementation

        Returns:
            str: The representation of the given key, in UltiSnips-style

        '''
        try:
            int(key)
        except ValueError:
            try:
                self.key_info['keyValues'][key]
            except KeyError:
                return self.get_keyword_value(key=key)

            return '${n}'.format(n=self.key_info['keyValues'][key])
        else:
            self.key_info['numbersUsed'].add(key)
            return '${n}'.format(n=key)

        raise NotImplementedError('This case should not be able to happen')

    def format_field(self, value, format_spec):
        try:
            return super(UltiSnipsTabstopFormatter, self).format_field(
                value=value, format_spec=format_spec)
        except ValueError:
            if value.startswith('$'):
                return '${' + value[1:] + ':' + format_spec + '}'


if __name__ == '__main__':
    print(__doc__)
