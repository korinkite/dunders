#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that imports all parse-friendly dunder methods into here.'''

''
# IMPORT STANDARD LIBRARIES
import inspect
import os

# IMPORT THIRD-PARTY LIBRARIES
import six

# IMPORT LOCAL LIBRARIES
from .parse_objects.parsers.basic import DiscoveryObject
from .parse_objects import parsers
from . import parse_objects


PARSE_METHOD_OBJS_LOCATION = \
    os.path.dirname(os.path.realpath(parsers.__file__))


def get_format_class_objects():
    '''Automatically discover and sort all found format method classes.'''
    class_objects = dict()
    discovery_paths = [PARSE_METHOD_OBJS_LOCATION]

    for path in discovery_paths:
        python_objects = import_package_files(
            package_path=path, namespace=parse_objects.__name__)

        for _, py_obj in six.iteritems(python_objects):
            try:
                # next((x for x in inspect.getmro(obj) if x == DiscoveryObject))
                next((obj for obj in inspect.getmro(type(py_obj))
                      if isinstance(py_obj, type(DiscoveryObject))))
            except StopIteration:
                pass
            else:
                class_objects[py_obj.name] = py_obj
    return class_objects


def import_package_files(package_path=os.path.dirname(__file__), namespace=''):
    '''Dynamically import all the public attributes of subdir python modules.
    Reference:
        https://stackoverflow.com/questions/5134893
    Args:
        package_path (str): The absolute path to a Python package/subpackage
        namespace (str): An additional prefix to put before the subpackage name
		                 Example: module.another_module

    Returns:
        dict[str: object]: The information to append to globals()
    '''
    globals_ = globals()
    locals_ = locals()
    package_name = os.path.basename(package_path)

    for filename in os.listdir(package_path):
        modulename, ext = os.path.splitext(filename)
        if modulename[0] != '_' and ext in ('.py', '.pyw'):
            # create a package relative subpackage name
            subpackage = '{0}.{1}'.format(package_name, modulename)
            if namespace:
                subpackage = namespace + '.' + subpackage

            module = __import__(subpackage, globals_, locals_, [modulename])
            modict = module.__dict__
            public_names = (modict['__all__'] if '__all__' in modict else
                            [name for name in modict if name[0] != '_'])
            globals_.update((name, modict[name]) for name in public_names)

    return globals_
