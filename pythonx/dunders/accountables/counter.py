#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''All classes/methods needed to assign unique IDs to Python objects.'''

# IMPORT STANDARD LIBRARIES
import collections

# IMPORT LOCAL LIBRARIES
from ..retro import single


class Counter(object):

    '''A individual object that is used to preserve its assigned object.'''

    def __init__(self, obj=None):
        '''Initialize the function instance and store its given code object.'''
        super(Counter, self).__init__()
        self.num = -1
        self.obj = obj

    def __str__(self):
        '''Display the stored object, with its info intact.

        If no object was passed, a positional (integer) value is substituted.
        Counter has no control over what this substituted number will be.
        That responsibility is managed by a state class (such as CounterState)
        which will assign a number to the object instance.

        '''
        if self.obj is None:
            # TODO : NEED to make this part. It's the most important part!
            raise NotImplementedError('Havent written this part yet')
        return '{' + self.obj + '}'


class CounterState(object):

    '''A state object that keeps record of objects and stores them in dict.

    The class was built to assist making object classes that is or contains
    object(s) that need to be dynamically built/counted.

    Example:
        This class is the crux of a system to build dynamic exapandable
        text snippets in Vim.

    '''

    __metaclass__ = single.SingletonContainer
    valid_counter_objects = (Counter, )

    def __init__(self):
        '''Create the metaclass instance and its counter database.'''
        super(CounterState, self).__init__()
        self.counter_dict = collections.OrderedDict()

    def add_counter(self, obj):
        '''Create a counter object and store it in the counter database.

        Args:
            obj (any): The object to add into the counter

        '''
        for expected_obj_type in self.valid_counter_objects:
            if isinstance(obj, expected_obj_type):
                break
        else:
            raise NotImplementedError(
                'Object: "{obj}" was not a valid object. Options were, "{opt}"'
                ''.format(obj=obj, opt=self.valid_counter_objects))

        self.counter_dict.setdefault('counters', [])
        self.counter_dict['counters'].append(obj)


if __name__ == '__main__':
    print(__doc__)
