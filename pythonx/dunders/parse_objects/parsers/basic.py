#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Build Python format-parse version of regular Python method classes.

These format-friendly methods can then be converted to your favorite snippet
program of choice.


Reference:
    https://github.com/RafeKettler/magicmethods/blob/master/magicmethods.markdown

'''

# IMPORT THIRD-PARTY LIBRARIES
# TODO : Make these imports relative
#
from dunders.accountables import counter
from dunders.parse_objects import base_method
from dunders.parse_objects import attribute


class DiscoveryObject(object):

    '''Used to help discover and load parse format objects.

    Any class that inherits from this class and is in one or more discovery
    folders will be automatically retrieved and built into a dict.

    '''

    name = 'DISCOVER'

    def __init__(self, *args, **kwargs):
        '''Initialize the object instance.'''
        super(DiscoveryObject, self).__init__()


class FormatNoReturnMethod(base_method.NoReturnMethod):
    def __init__(self):
        super(FormatNoReturnMethod, self).__init__()
        state = counter.CounterState()
        body_count = counter.Counter(obj=self.method.body)
        self.method.body = ''
        state.add_counter(body_count)


class FormatCommonMethod(base_method.StrMethod):

    '''A Python format-friendly representation of the __str__ method.'''

    def __init__(self):
        '''Initialize the function instance.

        This __init__ does an Indiana Jones swap of the body's return, which
        instead is swapped out for a Counter object.

        '''
        super(FormatCommonMethod, self).__init__()
        state = counter.CounterState()
        body_count = counter.Counter(obj=self.method.body.value)  # finish him!
        self.method.body.value = body_count
        state.add_counter(body_count)


class FormatOtherMethod(FormatCommonMethod):
    def __init__(self):
        super(FormatOtherMethod, self).__init__()
        arg = attribute.Arg(name='other')
        self.method.args.add_arg(arg)


class FormatKeyMethod(FormatCommonMethod):
    def __init__(self):
        super(FormatKeyMethod, self).__init__()
        arg = attribute.Arg(name='key')
        self.method.args.add_arg(arg)


class FormatNameMethod(FormatCommonMethod):
    def __init__(self):
        super(FormatNameMethod, self).__init__()
        arg = attribute.Arg(name='name')
        self.method.args.add_arg(arg)


class FormatKeyValueMethod(FormatKeyMethod):
    def __init__(self):
        super(FormatKeyValueMethod, self).__init__()
        arg = attribute.Arg(name='value')
        self.method.args.add_arg(arg)


# CONSTRUCTION AND INITIALIZATION
class FormatDelMethod(DiscoveryObject, FormatNoReturnMethod):

    name = 'del'


class FormatInitMethod(DiscoveryObject, FormatNoReturnMethod):

    name = 'init'


class FormatNewMethod(DiscoveryObject, FormatCommonMethod):

    name = 'new'

    def __init__(self):
        super(FormatNewMethod, self).__init__()
        for arg in ['mcs', 'clsname', 'bases', 'attrs']:
            arg = attribute.Arg(name=arg)
            self.method.args.add_arg(arg)











# ARITHMETIC
class FormatAddMethod(DiscoveryObject, FormatOtherMethod):

    name = 'add'


class FormatAndMethod(DiscoveryObject, FormatOtherMethod):

    name = 'and'


class FormatDivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'div'


class FormatDivModMethod(DiscoveryObject, FormatOtherMethod):

    name = 'divmod'


class FormatFloordivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'floordiv'


class FormatLshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'lshift'


class FormatTruedivModMethod(DiscoveryObject, FormatOtherMethod):

    name = 'truediv'


class FormatModMethod(DiscoveryObject, FormatOtherMethod):

    name = 'mod'


class FormatMulMethod(DiscoveryObject, FormatOtherMethod):

    name = 'mul'


class FormatOrMethod(DiscoveryObject, FormatOtherMethod):

    name = 'or'


class FormatPowMethod(DiscoveryObject, FormatOtherMethod):

    name = 'pow'


class FormatRshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rshift'


class FormatSubMethod(DiscoveryObject, FormatOtherMethod):

    name = 'sub'


class FormatXorMethod(DiscoveryObject, FormatOtherMethod):

    name = 'xor'




# REFLECTED ARTITHMETIC
class FormatRaddMethod(DiscoveryObject, FormatOtherMethod):

    name = 'radd'


class FormatRandMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rand'


class FormatRdivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'div'


class FormatRdivModMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rdivmod'


class FormatRfloordivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rfloordiv'


class FormatRlshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rlshift'


class FormatRtruedivModMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rtruediv'


class FormatRmodMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rmod'


class FormatRmulMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rmul'


class FormatRorMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ror'


class FormatRpowMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rpow'


class FormatRrshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rrshift'


class FormatRsubMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rsub'


class FormatRxorMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rxor'





# AUGMENTED ASSIGNMENT
class FormatIaddMethod(DiscoveryObject, FormatOtherMethod):

    name = 'iadd'


class FormatIandMethod(DiscoveryObject, FormatOtherMethod):

    name = 'iand'


class FormatIdivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'idiv'


class FormatIfloordivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ifloordiv'


class FormatIlshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ilshift'


class FormatImodMethod(DiscoveryObject, FormatOtherMethod):

    name = 'imod'


class FormatImulMethod(DiscoveryObject, FormatOtherMethod):

    name = 'imul'


class FormatIorMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ior'


class FormatIpowMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ipow'


class FormatIrshiftMethod(DiscoveryObject, FormatOtherMethod):

    name = 'irshift'


class FormatIsubMethod(DiscoveryObject, FormatOtherMethod):

    name = 'isub'


class FormatItruedivMethod(DiscoveryObject, FormatOtherMethod):

    name = 'itruediv'


class FormatIxorMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ixor'







class FormatCallMethod(DiscoveryObject, FormatNoReturnMethod):

    name = 'call'


class FormatCmpMethod(DiscoveryObject, FormatOtherMethod):

    name = 'cmp'


class FormatRcmpMethod(DiscoveryObject, FormatOtherMethod):

    name = 'rcmp'






# CONTAINER
class FormatContainsMethod(DiscoveryObject, FormatOtherMethod):

    name = 'contains'

    def __init__(self):
        super(FormatContainsMethod, self).__init__()
        self.method.body.value.obj = 'True'


class FormatDelitemMethod(DiscoveryObject, FormatKeyMethod):

    name = 'delitem'


class FormatGetitemMethod(DiscoveryObject, FormatKeyMethod):

    name = 'getitem'


class FormatIterMethod(DiscoveryObject, FormatCommonMethod):

    name = 'iter'


class FormatLenMethod(DiscoveryObject, FormatCommonMethod):

    name = 'len'


class FormatMissingMethod(DiscoveryObject, FormatKeyMethod):

    name = 'missing'


class FormatReversedMethod(DiscoveryObject, FormatCommonMethod):

    name = 'reversed'


class FormatSetitemMethod(DiscoveryObject, FormatKeyValueMethod):

    name = 'setitem'


class FormatDelattrMethod(DiscoveryObject, FormatNoReturnMethod):

    name = 'delattr'


class FormatEnterMethod(DiscoveryObject, FormatOtherMethod):

    name = 'enter'


class FormatEqMethod(DiscoveryObject, FormatOtherMethod):

    name = 'eq'


class FormatExitMethod(DiscoveryObject, FormatNoReturnMethod):

    name = 'exit'

    def __init__(self):
        super(FormatExitMethod, self).__init__()
        exec_type = attribute.Arg(name='exec_type')
        exec_val = attribute.Arg(name='exec_val')
        exec_traceback = attribute.Arg(name='traceback')
        self.method.args.add_arg(exec_type)
        self.method.args.add_arg(exec_val)
        self.method.args.add_arg(exec_traceback)






# COMVERSION
class FormatComplexMethod(DiscoveryObject, FormatCommonMethod):

    name = 'complex'


class FormatFloatMethod(DiscoveryObject, FormatCommonMethod):

    name = 'float'


class FormatIndexMethod(DiscoveryObject, FormatCommonMethod):

    name = 'index'


class FormatIntMethod(DiscoveryObject, FormatCommonMethod):

    name = 'int'


class FormatLongMethod(DiscoveryObject, FormatCommonMethod):

    name = 'long'


class FormatOctMethod(DiscoveryObject, FormatCommonMethod):

    name = 'oct'


class FormatTruncMethod(DiscoveryObject, FormatCommonMethod):

    name = 'trunc'



# DESCRIPTORS
class FormatGetMethod(DiscoveryObject, FormatCommonMethod):

    name = 'get'

    def __init__(self):
        super(FormatGetMethod, self).__init__()
        instance = attribute.Arg(name='instance')
        self.method.args.add_arg(instance)


class FormatSetMethod(FormatGetMethod):

    name = 'set'

    def __init__(self):
        super(FormatSetMethod, self).__init__()
        owner = attribute.Arg(name='owner')
        self.method.args.add_arg(owner)


class FormatDeleteMethod(FormatGetMethod):

    name = 'delete'






# UNARY OPERATIONS
class FormatAbsMethod(DiscoveryObject, FormatCommonMethod):

    name = 'abs'


class FormatCeilMethod(DiscoveryObject, FormatCommonMethod):

    name = 'ceil'


class FormatFloorMethod(DiscoveryObject, FormatCommonMethod):

    name = 'floor'


class FormatInvertMethod(DiscoveryObject, FormatCommonMethod):

    name = 'invert'


class FormatPosMethod(DiscoveryObject, FormatCommonMethod):

    name = 'pos'


class FormatNegMethod(DiscoveryObject, FormatCommonMethod):

    name = 'neg'


class FormatRoundMethod(DiscoveryObject, FormatCommonMethod):

    name = 'round'

    def __init__(self):
        super(FormatRoundMethod, self).__init__()
        number_arg = attribute.Arg(name='num')
        self.method.args.add_arg(number_arg)


class FormatGeMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ge'


class FormatGetattrMethod(DiscoveryObject, FormatNameMethod):

    name = 'getattr'


class FormatGetattributeMethod(DiscoveryObject, FormatNameMethod):

    name = 'getattribute'







# REPRESENTATION
class FormatDirMethod(DiscoveryObject, FormatCommonMethod):

    name = 'dir'


class FormatHashMethod(DiscoveryObject, FormatCommonMethod):

    name = 'hash'


class FormatFormatMethod(DiscoveryObject, FormatCommonMethod):

    name = 'format'


class FormatNonzeroMethod(DiscoveryObject, FormatCommonMethod):

    name = 'nonzero'


class FormatReprMethod(DiscoveryObject, FormatCommonMethod):

    name = 'repr'


class FormatSizeofMethod(DiscoveryObject, FormatCommonMethod):

    name = 'sizeof'


class FormatStrMethod(DiscoveryObject, FormatCommonMethod):

    name = 'str'


class FormatUnicodeMethod(DiscoveryObject, FormatCommonMethod):

    name = 'unicode'













# COMPARISON
class FormatCmpMethod(DiscoveryObject, FormatOtherMethod):

    name = 'cmp'


class FormatEqMethod(DiscoveryObject, FormatOtherMethod):

    name = 'eq'


class FormatGeMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ge'


class FormatGtMethod(DiscoveryObject, FormatOtherMethod):

    name = 'gt'


class FormatLeMethod(DiscoveryObject, FormatOtherMethod):

    name = 'le'


class FormatLtMethod(DiscoveryObject, FormatOtherMethod):

    name = 'lt'


class FormatNeMethod(DiscoveryObject, FormatOtherMethod):

    name = 'ne'









class FormatSetattrMethod(DiscoveryObject, FormatKeyValueMethod):

    name = 'setattr'


if __name__ == '__main__':
    print(__doc__)
