#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Represent the smallest pieces of a Python function as objects.'''


class NoneArgType(object):

    '''A Null Python object - used in-place of None where a NoneType is needed.

    If I wanted to test if an arg had a default value, you might normally
    test if the arg has nothing (None) for default value. But what if I meant
    to have that arg have default of None?

    The following class is used in place of None to remove that ambiguity.

    '''

    def __init__(self):
        '''Create the object instance.'''
        super(NoneArgType, self).__init__()


class Attr(object):

    '''A Python object to express a Property/Attribute/Arg.'''

    def __init__(self, name):
        '''Initialize the Python object.'''
        super(Attr, self).__init__()
        self.name = name

    def __str__(self):
        '''Show the attribute's string representation.'''
        return self.name


class Arg(Attr):

    '''A Python arg (like those you'd find in a function definition).'''

    null_type = NoneArgType

    def __init__(self, name, default_value=NoneArgType):
        '''Initialize the Python object.'''
        super(Arg, self).__init__(name=name)
        self.default_value = default_value

    def is_required(self):
        '''bool: Whether or not the arg has a default value.'''
        return self.default_value == self.null_type


class Args(object):

    '''An iterable of Arg objects.'''

    # pylint: disable=W0102
    def __init__(self, args=[]):
        '''Initialize the Python object.'''
        super(Args, self).__init__()
        args = args[:]
        self.arg_list = []

        for arg in args:
            self.add_arg(arg)

    def add_arg(self, arg):
        '''Add the arg object to the current object instance.

        When the object is printed with str() later, these args are displayed.

        Args:
            arg (Arg): The argument to add to the arg list

        '''
        if not isinstance(arg, Arg):
            raise NotImplementedError('Implement other object types')
        self.arg_list.append(arg)

    def __str__(self):
        '''Display all the args in a list.'''
        return ', '.join([str(x) for x in self.arg_list])


if __name__ == '__main__':
    print(__doc__)
