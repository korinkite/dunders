#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Describe a return statement in Python as an object.'''

# IMPORT LOCAL LIBRARIES
from . import attribute


class Return(object):

    '''A basic python object that signifies a return statement.'''

    def __init__(self, value=attribute.NoneArgType):
        '''Initialize the function instance.

        Args:
            value (any): The object to return

        '''
        super(Return, self).__init__()
        self.value = value

    def __str__(self):
        '''Print the return statement with its contents, if any.'''
        return_str = 'return'
        if not isinstance(self.value, attribute.NoneArgType):
            return_str += ' {0}'.format(str(self.value))

        return return_str


if __name__ == '__main__':
    print(__doc__)
