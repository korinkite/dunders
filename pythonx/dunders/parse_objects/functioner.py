#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A Python object representation of a Python function (how meta!).'''


class Method(object):

    '''A basic Python objec to represent a Python method or function.'''

    # pylint: disable=W0102
    def __init__(self, name, body='', args=[], config=dict()):
        '''Initialize the function instance.

        Add body/args info if-wanted but it is not required.

        Args:
            name (str): The name of the function/method
            body (?): The body of the function
            args (?): All of the args that go in the function definition line
            config (dict): An optional configuration dict that overrides the
                default behavior of the class. For example, if in the dict,
                a key such as "preferTabs": True, then tabs will be used
                instead of spaces when the function is printed

        '''
        # TODO: Finish this class's docstring
        super(Method, self).__init__()
        self._real_name = name
        self.body = body
        self.args = args[:]

    def get_name(self):
        '''Display the function's real name and modify it, if needed.

        Most classes/subclasses modify the name before displaying, such as
        making a non-compliant name PEP8-compliant or making a __.+__ method.

        Returns:
            str: The name of the function

        '''
        return self.get_real_name()

    def get_real_name(self):
        '''str: The real name of the function, unmodified.'''
        return self._real_name

    def get_definition_line(self):
        '''str: The top Python "def" line of a method.'''
        output_str = 'def '
        output_str += self.get_name()
        output_str += '({args}):'.format(args=str(self.args))
        return output_str

    def __str__(self):
        '''The string representation of the current class.'''
        output_str = self.get_definition_line()
        output_str += '\n    {body}'.format(body=str(self.body))
        return output_str


class DunderMethod(Method):

    '''A __.+__ variant of a regular Python funcion.'''

    # pylint: disable=W0102
    def __init__(self, name, body='', args=[]):
        '''Initialize the function instance.'''
        super(DunderMethod, self).__init__(name=name, body=body, args=args)

    def get_name(self):
        '''Add "__" prefix/suffix to function name.'''
        return '__{func}__'.format(func=super(DunderMethod, self).get_name())


if __name__ == '__main__':
    print(__doc__)
