#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Create the most basic dunder_methods, those requiring no args.'''

# IMPORT LOCAL LIBRARIES
from . import functioner
from . import attribute
from . import body


# TODO : make this an abstract base class
class CommonDunderMethod(object):

    '''Basic class that holds common points of all Dunder methods.'''

    name = 'COMMON'

    def __init__(self):
        '''Initialize the Python object.'''
        super(CommonDunderMethod, self).__init__()
        self.method = self.get_method()
        self.self_arg = attribute.Arg(name='self')

    @classmethod
    def get_method(cls):
        '''<functioner.DunderMethod>: The method object to display.'''
        return functioner.DunderMethod(name=cls.name)

    def __str__(self):
        '''The visual representation of this function.'''
        return str(self.method)


class NoReturnMethod(CommonDunderMethod):

    '''A method that has no return/yield and may/may not contain a body.'''

    def __init__(self):
        '''Initialize the Python object.'''
        super(NoReturnMethod, self).__init__()
        self.method.args = self.get_method_args()
        self.method.body = ''

    def get_method_args(self):
        '''<attribute.Args>: The iterable of args for the method.'''
        args = attribute.Args()
        args.add_arg(self.self_arg)
        return args


class StrMethod(NoReturnMethod):

    '''The __str__ method, as a Python object.'''

    name = 'str'

    def __init__(self):
        '''Initialize the Python object.'''
        super(StrMethod, self).__init__()
        self.method.body = body.Return(value="''")


class ReprMethod(NoReturnMethod):

    '''The __repr__ method, as a Python object.'''

    name = 'repr'

    def __init__(self):
        '''Initialize the Python object.'''
        super(ReprMethod, self).__init__()


if __name__ == '__main__':
    print(__doc__)
