#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import ast
import re

# IMPORT LOCAL LIBRARIES
from ..parse_objects.attribute import Arg


class ArgDocstringDisplay(object):
    def __init__(self, arg):
        super(ArgDocstringDisplay, self).__init__()
        self.arg = arg

    def __str__(self):
        output_str = '{' + self.arg.name + '} '
        if not self.arg.is_required():
            obj_type = get_string_obj_type(self.arg.default_value)
            if not obj_type:
                # Fallback to the original object if no object type was found
                obj_type = self.arg.default_value

            output_str += '(:obj:`{%s}`, optional)' % obj_type
        else:
            output_str += '({})'

        output_str += ': {}'
        return output_str


def get_string_obj_type(obj):
    try:
        return type(ast.literal_eval(obj)).__name__
    except Exception:
        pass


def get_args_objects(definition_line):
    args = definition_line[definition_line.find('(') + 1:
                           definition_line.rfind(')')]
    all_args = []

    arg_comp = re.compile(
        '''
        (?P<name>[a-zA-Z0-9_]+)(?:(?:\ +)?=(?:\ +)?(?P<value>.+))?
        ''', re.VERBOSE)

    for match in args.split(','):
        match = match.strip()
        arg_info = re.match(arg_comp, match)

        default_value = arg_info.group('value')
        if not default_value:
            default_value = Arg.null_type

        attr = Arg(name=arg_info.group('name'), default_value=default_value)

        all_args.append(attr)

    return all_args


def test_string_thing():
    definition = 'def some_var(something, ttt=8)'
        # 'def some_var(something, tttt, yasdf=10)'
        # 'def some_var(something, tttt, yasdf=(10, 7), rawr="aer")'
    output_str = ''
    args = get_args_objects(definition)
    if not args:
        return ''

    output_str += 'Args:\n'

    for arg in args:
        doc_arg = ArgDocstringDisplay(arg)
        # TODO : replace with config info
        output_str += '    ' + str(doc_arg) + '\n'

    print(output_str)


if __name__ == '__main__':
    test_string_thing()
