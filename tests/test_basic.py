#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Test all of the basic dunder methods.

These methods are typically simple and don't vary much between each other.

'''

# IMPORT THIRD-PARTY LIBRARIES
import dunders.ultisnips_build as ultisnips_build
import dunders.manager


#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Allows for multiple assertions in a single Python test (use wisely).'''

# IMPORT STANDARD LIBRARIES
import sys
import unittest


# Works with unittest in Python 2.7
class ExpectingTestCase(unittest.TestCase):

    '''A modified test suite that can run more than one assert in a test.'''

    def run(self, result=None):
        self._result = result
        self._num_expectations = 0
        super(ExpectingTestCase, self).run(result)

    def _fail(self, failure):
        try:
            raise failure
        except failure.__class__:
            self._result.addFailure(self, sys.exc_info())

    def expect_true(self, a, msg=''):
        if not a:
            msg = '({}) Expected {} to be True.' \
                  ''.format(self._num_expectations, a) + msg
            self._fail(self.failureException(msg))
        self._num_expectations += 1

    def expect_false(self, a, msg=''):
        if a:
            msg = '({}) Expected {} to be False.' \
                  ''.format(self._num_expectations, a) + msg
            self._fail(self.failureException(msg))
        self._num_expectations += 1

    def expect_equal(self, a, b, msg=''):
        if a != b:
            msg = '({}) Expected {} to equal {}.' \
                  ''.format(self._num_expectations, a, b) + msg
            self._fail(self.failureException(msg))
        self._num_expectations += 1


class NoArgDundersTestCase(ExpectingTestCase):

    '''Dunder methods with no arguments, like __str__/__repr__/__del__.'''

    @classmethod
    def get_format_method(cls, name):
        format_classes = dunders.manager.get_format_class_objects()
        return str(format_classes[name]())

    @classmethod
    def get_quick_method(cls, name):
        '''Convenienve method to get and build an output string snippet.'''
        parse_method = cls.get_format_method(name=name)
        formatter = ultisnips_build.UltiSnipsTabstopFormatter()
        return formatter.format(parse_method)

    def test_simple_parse_methods_build(self):
        '''Test predictable parse structures, like__str__/__repr__.'''
        method_names = \
            [
                'str',
                'repr',
            ]
        for name in method_names:
            expected_output = \
'''\
def __%s__(self):
    return {''}\
''' % name
            generated_output = self.get_format_method(name)
            self.expect_equal(generated_output, expected_output)

    def test_simple_return_methods_build(self):
        '''Test predictable structures, like__str__/__repr__.'''
        method_names = \
            [
                'str',
                'repr',
            ]
        for name in method_names:
            expected_output = \
'''\
def __%s__(self):
    return ${1:''}\
''' % name
            generated_output = self.get_quick_method(name)
            self.expect_equal(generated_output, expected_output)


if __name__ == '__main__':
    print(__doc__)
